import * as React from "react";
import BookingForm from "../../PageElements/Form/BookingForm";
import Page from "../Page/Page";
import {content} from "./content";


const Prenota = (props: any) => {
    let handleCheckinDate = (checkin: Date) => {
        setDate(checkin);
    }
    let handleNights = (nights: number) => {
        setNights(nights);
    }

    const [date, setDate] = React.useState(new Date());
    const [nights, setNights] = React.useState(3);

    return (
        <div>
            <Page contentArray = {content} language = {props.language}/>
            <BookingForm
                checkin= {date}
                onCheckinChange={handleCheckinDate}
                nights={nights}
                onNightsChange={handleNights}
                language = {props.language}
            />
        </div>
    );
}

export default Prenota;