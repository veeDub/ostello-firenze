export let English = '<h3 class = \'mainTitle\'>Book Online</h3>' +
    '<p class=\'leftP\'>You can use this search form to find all the updated information on prices, offers and availability of the Villa Camerata hostel and book online with immediate confirmation your room in Florence.</p>' +
    '<p class=\'leftP\'>To book a room online (or a bed in a dormitory) you will need to pay a 10% deposit by credit card, while the remaining 90% can be paid upon arrival at the hostel. The booking procedure is secure and guaranteed.</p>'

export let Italian = '<h3 class = \'mainTitle\'>Prenota Online</h3>' +
    '<p class=\'leftP\'>Puoi utilizzare questa maschera di ricerca per trovare tutte le informazioni aggiornate su prezzi, offerte e disponibilità dell\'ostello Villa Camerata e prenotare online con conferma immediata la tua camera a Firenze.</p>' +
    '<p class=\'leftP\'>Per prenotare online una stanza (o un letto in camerata) dovrai versare un deposito del 10% tramite carta di credito, mentre il rimanente 90% potrai saldarlo all\'arrivo in ostello. La procedura di prenotazione è sicura e garantita.</p>'

export let Croatian = '<h3 class = \'mainTitle\'>Rezervirajte Online</h3>' +
    '<p class=\'leftP\'>Pomoću ovog obrasca pretraživanja možete pronaći sve ažurirane informacije o cijenama, ponudama i dostupnosti hostela Villa Camerata i rezervirati svoje sobe u Firenci online i putem trenutne potvrde.</p>' +
    '<p class=\'leftP\'>Za online rezervaciju sobe (ili kreveta u spavaonici) morat ćete platiti 10% depozita kreditnom karticom, a preostalih 90% možete platiti po dolasku u hostel. Postupak rezervacije je siguran i zagarantiran.</p>'

export const Turkish = '<h3 class = \'mainTitle\'> Online Rezervasyon\n</h3>' +
    '<p class=\'leftP\'>Villa Camerata Hostel\'in fiyatları, teklifleri ve bulunabilirliği hakkındaki tüm güncel bilgileri bulmak için bu arama formunu kullanabilir ve Floransa\'daki odanızı hemen onaylayarak online rezervasyon yapabilirsiniz.\n</p>' +
    '<p class=\'leftP\'>Online olarak oda rezervasyonu yapmak için (veya yatakhanede bir yatak) kredi kartıyla % 10 depozito ödemeniz gerekirken, kalan % 90\'ı hostele varışta ödeme yapmanız gerekmektedir. Rezervasyon işlemi güvenli ve garantilidir.</p>'

export const Portuguese = '<h3 class = \'mainTitle\'> Reserva en Línea\n</h3>' +
    '<p class=\'leftP\'>Puedes utilizar este formulario de búsqueda para encontrar la información más reciente sobre precios, ofertas y disponibilidad del albergue Villa Camerata y reservar online con confirmación instantánea de tu habitación en Florencia.</p>' +
    '<p class=\'leftP\'>Para reservar una habitación (o una cama en un dormitorio) en línea tendrás que pagar un depósito del 10% con tarjeta de crédito, mientras que el 90% restante se puede pagar a la llegada al albergue. El procedimiento de reserva es seguro y garantizado.</p>'


export const ItalianTitle = "Prenota online la tua stanza lowcost a Firenze | Ostello Villa Camerata"
export const EnglishTitle = "Book online your lowcost room in Florence | Villa Camerata Hostel"
export const CroatianTitle = "Rezervirajte online svoju jeftinu sobu u Firenci | Hostel Villa Camerata"
export const TurkishTitle = "Floransa'daki düşük bütçeli odanız için online rezervasyon yapın | Hostel Villa Camerata"
export const PortugueseTitle = "Reserve online seu quarto de baixo custo em Florença | Hostel Villa Camerata"

export const ItalianDesc = "Prenota online le migliori offerte per dormire lowcost a Firenze grazie alle tante soluzioni disponibili all’Ostello Villa Camerata."
export const EnglishDesc = "Book online the best offers to sleep lowcost in Florence thanks to the many solutions available at Villa Camerata Hostel."
export const CroatianDesc = "Rezervirajte putem interneta najbolje ponude za spavanje s niskim cijenama u Firenci zahvaljujući brojnim rješenjima koja su dostupna u Villa Camerata Hostel."
export const TurkishDesc = "Villa Camerata Hostel'de mevcut olan birçok çözüm sayesinde, Floransa'da yatmak için en iyi teklifleri online olarak ayırtın."
export const PortugueseDesc = "Reserve online as melhores ofertas para dormir com preços baixos em Florença, graças às muitas soluções disponíveis no Villa Camerata Hostel."

export const content = {"Portuguese" : Portuguese, "PortugueseTitle" : PortugueseTitle, "PortugueseDesc" : PortugueseDesc, "EnglishTitle" : EnglishTitle, "English" : English, "EnglishDesc" : EnglishDesc, "ItalianTitle" : ItalianTitle, "Italian" : Italian, "ItalianDesc" : ItalianDesc, "CroatianTitle" : CroatianTitle, "Croatian" : Croatian, "CroatianDesc" : CroatianDesc, "TurkishTitle" : TurkishTitle, "Turkish" : Turkish, "TurkishDesc" : TurkishDesc}