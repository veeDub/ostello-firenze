export let Italian = "<h3 class = 'mainTitle'>Galleria</h3>" +
    "<p class = \"leftP\">La Toscana è semplicemente meravigliosa e Firenze è famosa in tutto il mondo come una delle città più belle in cui vivere: ecco perché per le tue vacanze non ti devi accontentare di un hotel o un ostello qualsiasi...meriti di più, meriti una Villa Ostello!</p>" +
    "<p class = \"leftP\">L'ostello di Firenze si trova in una vera villa e in questa pagina potrai sbirciare qualche foto scattata dal nostro staff...buona photogallery!</p>"

export let English = "<h3 class = 'mainTitle'>Photo Gallery</h3>" +
    "<p class = \"leftP\">Tuscany is simply amazing and Florence is wordlwide known as one of the most beautiful cities to live: that's why your holiday in Italy can't be based in a standard hotel or B&B...you deserve more, you deserve a Villa Hostel!</p>" +
    "<p class = \"leftP\">We are located in a real villa and in this page you can take a look on some photos taken from our hostel staff in our property, in Florence and nearby in Tuscany...enjoy this photogallery!</p>"

export let Croatian = "<h3 class = 'mainTitle'>Fotogalerija</h3>" +
    "<p class = \"leftP\">Toskana je jednostavno nevjerojatna, a Firenca je nadaleko poznata kao jedan od najljepših gradova za život: zato se vaš odmor u Italiji ne može nalaziti u standardnom hotelu ili pansionu ... zaslužujete više, zaslužili ste Villa Hostel!</p>" +
    "<p class = \"leftP\">Nalazimo se u pravoj vili i na ovoj stranici možete pogledati neke fotografije snimljene od osoblja našeg hostela u našem posjedu, u Firenci i u blizini Toskane ... uživajte u ovoj fotogaleriji!</p>"

export const Turkish = "<h3 class = 'mainTitle'>Galeri</h3>" +
    "<p class = \"leftP\">Toskana tek kelimeyle şaşırtıcı ve Floransa dünya çapında yaşanacak en güzel şehirlerden biri olarak biliniyor: bu yüzden İtalya'daki tatilinizi standart bir otel veya B&B ile geçiremezsiniz... Daha fazlasını, Villa Hostel'i hak ediyorsunuz!</p>" +
    "<p class = \"leftP\">Gerçek bir villada yer alıyoruz ve bu sayfada, Floransa'da Toskana yakınlarında bulunan hostelimizin fotoğraflarına göz atabilirsiniz... Fotoğraf galerisinin tadını çıkarın!\n</p>"

export const Portuguese = "<h3 class = 'mainTitle'>Galeria</h3>" +
    "<p class = \"leftP\">La Toscana es simplemente maravillosa y Florencia es famosa en todo el mundo como una de las ciudades más bellas en las que vivir: es por eso que para tus vacaciones no tienes que conformarte con un hotel o cualquier hostal.... mereces más, mereces un Hostal Villa.</p>" +
    "<p class = \"leftP\">El albergue de Florencia se encuentra en una auténtica villa y en esta página podrás ver algunas fotos tomadas por nuestro personal.... ¡una buena galería de fotos!</p>"


export const ItalianTitle = "Guarda le foto dell’Ostello della Gioventù Villa Camerata a Firenze"
export const EnglishTitle = "Look at the pictures of the Villa Camerata Youth Hostel in Florence"
export const CroatianTitle = "Pogledajte slike omladinskog hostela Villa Camerata u Firenci"
export const TurkishTitle = "Villa Camerata Youth Hostel Florence fotoraflarına bakın"
export const PortugueseTitle = "Veja as fotos do Albergue da juventude Villa Camerata em Florença"

export const ItalianDesc = "Visita la fotogallery dell’ostello Villa Camerata e vedrai che per il tuo soggiorno a Firenze potrai contare su belle camere private o dormitori condivisi, il tutto in una vera villa circondata dal verde."
export const EnglishDesc = "Visit the photo gallery of the Villa Camerata hostel and you will see that for your stay in Florence you can count on beautiful private rooms or shared dormitories, all in a real villa surrounded by a green park."
export const CroatianDesc = "Posjetite fotogaleriju hostela Villa Camerata i vidjet ćete da za svoj boravak u Firenci možete računati na prekrasne privatne sobe ili zajedničke spavaonice, sve u pravoj vili okruženoj zelenilom."
export const TurkishDesc = "Villa Camerata hostelin fotoğraf galerisini ziyaret edin; Floransa'daki konaklamanız için yeşilliklerle çevrili gerçek bir villada, güzel özel odalara veya ortak yurtlara güvenebileceğinizi göreceksiniz."
export const PortugueseDesc = "Visite a galeria de fotos do albergue Villa Camerata e verá que, para a sua estadia em Florença, você pode contar com belos quartos privados ou dormitórios compartilhados, tudo em uma verdadeira villa cercada por vegetação."

export const content = {"PortugueseDesc" : PortugueseDesc, "PortugueseTitle" : PortugueseTitle, "Portuguese" : Portuguese, "EnglishTitle" : EnglishTitle, "English" : English, "EnglishDesc" : EnglishDesc, "ItalianTitle" : ItalianTitle, "Italian" : Italian, "ItalianDesc" : ItalianDesc, "CroatianTitle" : CroatianTitle, "Croatian" : Croatian, "CroatianDesc" : CroatianDesc, "TurkishTitle" : TurkishTitle, "Turkish" : Turkish, "TurkishDesc" : TurkishDesc}
