import * as React from 'react'
import "../../index.css"
import Gallery from "../../PageElements/Gallery/Gallery";

import Page from "../Page/Page";
import {content} from "./content";

const Galleria = (props:any) => {
    return (
        <div >
            <Page contentArray = {content} language = {props.language}/>
            <Gallery />
        </div>
    )
}

export default Galleria