import * as React from "react";

const NotFoundPage: React.FC = () => {
    return (
            <div className = "innerTextDiv">
                <div className="error-page">
                    <h3 className = 'mainTitle'>Sorry, this page cannot be found</h3>
                </div>
            </div>
    );
};

export default NotFoundPage;
