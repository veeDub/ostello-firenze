import * as React from 'react'
import Page from "../Page/Page";
import {content} from "./content";

const Ostello = (props: any): any => {
    return (
        <div>
            <Page contentArray = {content} language = {props.language}/>
        </div>
    )
}

export default Ostello