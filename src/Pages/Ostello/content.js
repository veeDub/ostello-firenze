export let Italian =
    "<h3 class = 'mainTitle'>Ostello di Firenze</h3>" +
    "<p class='leftP'>Immagina una vacanza in Toscana, in una villa del 15° secolo, decorata con stucchi e affreschi e circondata da un placido parco...non è un sogno irraggiungibile, questo è quello che troverai al Villa Camerata, l'ostello della gioventù di Firenze, dove le stanze per gli ospiti sono economiche e dotate di tutti i comfort!\n </p>" +
    "\n" +
    "<p class='leftP'>Il \"Villa Camerata\" è l'ostello ufficiale di Firenze (fa parte dell'associazione HI/AIG) ed è l'unico alloggio in città dove puoi trovare una bella atmosfera informale, buoni servizi e prezzi economici. Se prenoti una stanza in questa struttura ti ritroverai immerso nella magia della città più romantica, dove poeti e artisti da tutto il mondo hanno lasciato il loro cuore.\n</p>" +
    "\n" +
    "<p class='leftP'>L'ostello di Firenze è sempre arricchito da una vivace folla di backpackers che arrivano da ogni angolo del pianete per godersi una vacanza indimenticabile in Toscana: la struttura si trova ai piedi delle colline di Fiesole (una piccola e incantevole cittadina, famosa per il suo Teatro Romano e per il suo museo archeologico) e in zona potrai trovare tutto quello che serve. C'è una piscina pubblica, uno stadio di calcio e il Nelson Mandela forum dove si tengono importanti concerti, spettacoli teatrali ed eventi culturali. Soggiornerai distante dal caos del centro di Firenze, ma al tempo stesso lo potrai raggiungere in tutta facilità!\n </p>" +
    "\n" +
    "<img  class = 'center' src=\"../../Images/florence-hostel-villa-camerata.jpg\" alt=\"Ostello della gioventù di Firenze Villa Camerata\">" +
    "\n" +
    "<p class='leftP'>Conoscerai il nostro staff, sempre disponibile ad accoglierti e ad aiutarti a scoprire gli itinerari più suggestivi e nascosti di Firenze, la sola città dove ogni angolo trasuda storia e arte!\n </p>" +
    "\n" +
    "<p class='leftP'>Inizia la tua giornata con una deliziosa colazione servita nella sala da pranzo con vista panoramica e sarai subito pronto a mescolarti con i locali e goderti ospitalità e calore dei fiorentini doc!\n </p>" +
    "\n" +
    "<p class='leftP'>Oltre a poterti rilassare nel paradisiaco parco che circonda l'ostello, avrai a disposizione numerose sale comuni in cui mescolarti con gli altri ospiti provenienti da tutto il mondo e fare nuove amicizie. Il tutto al riparo dallo stress dei flussi turistici di Firenze e con tanti mezzi pubblici a disposizione per raggiungere comodamente il centro città.</p>" +
    "<img  class = 'center' src=\"../../Images/florence-hostel-common-area.jpg\" alt=\"La reception dell'ostello di Firenze\">"

export let English =
    "<h3 class = 'mainTitle'>About Florence Hotel</h3>" +
    "<p class='leftP'>Imagine a vacation in Tuscany, in a 15th century villa, decorated with stuccoes and frescoes and surrounded by a peaceful park...this is not an unreachable dream, this is what you can find at Villa Camerata Hostel, a lowcost hostel in Florence, but with all modern comforts.</p>" +
    "\n" +
    "<p class='leftP'>Ostello Villa Camerata is the official hostel in Florence (it's part of the HI/AIG association) and it's the only place in town where you can find a nice atmosphere, good services and cheap prices. Book a room here and you'll dive into the magic of the most romantic city, where poets and artists from all over the world have left their hearts.\n</p>" +
    "\n" +
    "<p class='leftP'>Florence Hostel is always enriched by a lively crowd of guests coming from all over the world to enjoy an unforgettable holiday in Tuscany: we're located at the bottom of Fiesole hills (a little enchanting town, popoular for its famous Roman Theatre and for the archaeological museum) and nearby you will find all you need. There's a public swimming pool, a soccer stadium and the Nelson Mandela forum where you can take part of important concerts, theatrical shows and cultural events. You will stay away from the messy and noisy of Florence downtown, but at the same time very close to city center!\n </p>" +
    "<img  class = 'center'  src=\"../../Images/florence-hostel-villa-camerata.jpg\" alt=\"Florence Youth Hostel Villa Camerata\">\n" +
    "<p class='leftP'>You will meet our staff, always ready to welcome you and suggest you the most unique and secret itineraries of Florence, the only city where every corner is history and art!\n </p>" +
    "\n" +
    "<p class='leftP'>Begin your day with a delicious breakfast served in the dining room with a wonderful panoramic view and you will be ready to get involved with the locals, and from the warmth and suggestion of our wonderful city.\n </p>" +
    "\n" +
    "<p class='leftP'>Apart from enjoying the relaxation in the garden and the entertainment offered by the hostel, here it's possible to meet young people and make friends from all over the world. Away from the noise of the city but very close to the historical center of Florence, easily reachable by public transport. \n  </p> " +
    "<img  class = 'center' src=\"../../Images/florence-hostel-common-area.jpg\" alt=\"Florence Hostel reception\">"

export const Croatian = "<h3 class = 'mainTitle'>O firentinskom hostelu</h3>" +
    "<p class='leftP'>Zamislite odmor u Toskani, u vili iz 15. stoljeća, ukrašenu štukaturama i freskama i okruženu mirnim parkom ... ovo nije neostvarivi san, to možete pronaći u Hotelu Villa Camerata, niskobudžetnom hostelu u Firenci, ali uz sve moderne pogodnosti.\n</p>" +
    "\n" +
    "<p class='leftP'>Ostello Villa Camerata je službeni hostel u Firenci (dio je udruge HI / AIG) i jedino je mjesto u gradu u kojem možete pronaći lijepu atmosferu, dobre usluge i jeftine cijene. Rezervirajte sobu ovdje i zaronit ćete u čari najromantičnijeg grada, gdje su pjesnici i umjetnici iz cijelog svijeta napustili svoje srce.</p>" +
    "\n" +
    "<p class='leftP'>Hostel Florence uvijek je obogaćen živahnom gomilom gostiju koji dolaze iz cijelog svijeta kako bi uživali u nezaboravnom odmoru u Toskani: nalazimo se na dnu brda Fiesole (malo očaravajuće mjesto, popoular zbog svog poznatog rimskog kazališta i za arheološki muzej) i u blizini ćete naći sve što trebate. Tu su javni bazen, nogometni stadion i forum Nelson Mandela gdje možete sudjelovati na važnim koncertima, kazališnim predstavama i kulturnim događanjima. Ostat ćete daleko od neuredne i bučne centra Firenze, ali istovremeno vrlo blizu centra grada! </p>" +
    "\n" +
    "<img  class = 'center' src=\"../../Images/florence-hostel-villa-camerata.jpg\" alt=\"Ostello della gioventù di Firenze Villa Camerata\">" +
    "\n" +
    "<p class='leftP'>Upoznat ćete naše osoblje, uvijek spremno da vas ugosti i predloži vam najunikatnije i najtajnije itinerere Firenze, jedini grad u kojem je svaki kutak povijest i umjetnost!\n </p>" +
    "\n" +
    "<p class='leftP'>Započnite dan ukusnim doručkom posluženim u blagovaonici s prekrasnim panoramskim pogledom i bit ćete spremni družiti se s mještanima, a iz topline i prijedloga našeg prekrasnog grada. </p>" +
    "\n" +
    "<p class='leftP'>Osim uživanja u opuštanju u vrtu i zabavi koju nudi hostel, ovdje je moguće upoznati mlade ljude i sprijateljiti se sa svim prijateljima iz cijelog svijeta. Daleko od gradske buke, ali vrlo blizu povijesne jezgre Firenze, lako dostupno javnim prijevozom.</p>" +
    "<img  class = 'center' src=\"../../Images/florence-hostel-common-area.jpg\" alt=\"La reception dell'ostello di Firenze\">"

export const Turkish = "<h3 class = 'mainTitle'>Florance Hostel Hakkında\n</h3>" +
    "<p class='leftP'>Toskana'da, 15. yüzyıldan kalma bir villada fresklerle süslenmiş ve huzurlu bir parkla çevrili bir tatil hayal edin ... Bu ulaşılmaz bir hayal değil, Floransa'daki ekonomik bir hostel olan Villa Camerata Hostel'de modern konforla bulabilirsiniz.\n</p>" +
    "\n" +
    "<p class='leftP'>Ostello Villa Camerata, Floransa'da resmi bir hosteldir (HI / AIG birliğinin bir parçasıdır). Güzel bir atmosfer, iyi hizmetler ve ucuz fiyatlar bulabileceğiniz şehirdeki tek yerdir. Burada bir oda ayırtın ve dünyanın her yerinden şairlerin ve sanatçıların yüreklerini bıraktığı en romantik şehrin büyüsüne dalın.</p>" +
    "\n" +
    "<p class='leftP'>Florence Hostel, Toskana'da unutulmaz bir tatilin tadını çıkarmak için dünyanın her yerinden gelen insanlar tarafından her zaman zenginleştirilmiştir: Fiesole tepelerinin altında (küçük, büyüleyici bir kasaba, ünlü Roma Tiyatrosu, arkeoloji müzesi) ve yakınınızda ihtiyacınız olan her şeyi bulacaksınız. Halka açık bir yüzme havuzu, bir futbol stadyumu ve önemli konserlere, tiyatro gösterilerine ve kültürel etkinliklere katılabileceğiniz Nelson Mandela forumu var. Floransa şehir merkezinin dağınık ve gürültüsünden uzak duracaksınız, aynı zamanda şehir merkezine de çok yakın olacaksınız!</p>" +
    "\n" +
    "<img  class = 'center' src=\"../../Images/florence-hostel-villa-camerata.jpg\" alt=\"Ostello della gioventù di Firenze Villa Camerata\">" +
    "\n" +
    "<p class='leftP'>Her zaman sizi ağırlamaya hazır, her köşesinde tarih ve sanat olan tek şehir olan, Floransa'nın en eşsiz ve gizli güzergahlarını önerecek olan personelimizle tanışacaksınız!\n</p>" +
    "\n" +
    "<p class='leftP'>Güne harika bir panoramik manzaralı yemek salonunda servis edilen leziz bir kahvaltı ile başlayın; yöre halkına harika şehrimizin sıcaklığı ve önerisiyle  katılmaya hazır olun.\n</p>" +
    "\n" +
    "<p class='leftP'>Bahçedeki rahatlamanın ve hostelin sunduğu eğlencenin tadını çıkarmanın yanı sıra, burada gençlerle tanışmak, dünyanın her yerinden arkadaşlar edinmek mümkün. Şehrin gürültüsünden uzakta, ancak Floransa'nın tarihi merkezine çok yakın, toplu taşıma araçları ile kolayca ulaşılabilir.\n</p>" +
    "<img  class = 'center' src=\"../../Images/florence-hostel-common-area.jpg\" alt=\"La reception dell'ostello di Firenze\">"

export const Portuguese = "<h3 class = 'mainTitle'>Ostello di Firenze</h3>" +
    "<p class='leftP'>Imagina unas vacaciones en la Toscana, en una villa del siglo XV, decorada con mosaicos y frescos y rodeada de un tranquilo parque... no es un sueño inalcanzable, esto es lo que encontrarás en Villa Camerata, el albergue juvenil de Florencia, donde las habitaciones de huéspedes son muy asequibles y están equipadas con todo tipo de comodidades.</p>" +
    "\n" +
    "<p class='leftP'>El \"Villa Camerata\" es el albergue oficial de Florencia (parte de la asociación HI/AIG) y es el único alojamiento de la ciudad donde se puede encontrar un buen ambiente informal, buenos servicios y precios económicos. Si reservas una habitación en esta propiedad, te encontrarás inmerso en la magia de la ciudad más romántica, donde poetas y artistas de todo el mundo han dejado sus corazones.</p>" +
    "\n" +
    "<p class='leftP'> L'ostello di Firenze siempre se enriquece con una animada multitud de mochileros que vienen de todos los rincones del planeta para disfrutar de unas vacaciones inolvidables en la Toscana: la propiedad se encuentra al pie de las colinas de Fiesole (una pequeña y encantadora ciudad, famosa por su teatro romano y su museo arqueológico) y en la zona hay todo lo que se necesita para disfrutar de la vida en la Toscana. Hay una piscina pública, un estadio de fútbol y el foro Nelson Mandela, donde se celebran importantes conciertos, representaciones teatrales y eventos culturales. Te mantendrás alejado del caos del centro de Florencia, pero al mismo tiempo podrás llegar a él fácilmente.</p>" +
    "\n" +
    "<img  class = 'center' src=\"../../Images/florence-hostel-villa-camerata.jpg\" alt=\"Ostello della gioventù di Firenze Villa Camerata\">" +
    "\n" +
    "<p class='leftP'>Conocerás a nuestro personal, siempre disponible para darte la bienvenida y ayudarte a descubrir los itinerarios más bellos y escondidos de Florencia, la única ciudad donde cada rincón ¡destila historia y arte!</p>" +
    "\n" +
    "<p class='leftP'>Empieza el día con un delicioso desayuno servido en el comedor con vistas panorámicas y estarás listo para mezclarte con los locales y ¡disfrutar de la hospitalidad y calidez de los florentinos!</p>" +
    "\n" +
    "<p class='leftP'>Además de relajarte en el parque paradisíaco que rodea el albergue, tendrás numerosas salas comunes donde podrás mezclarte con otros huéspedes de todo el mundo y hacer nuevos amigos. Todo esto está protegido del estrés de los turistas de Florencia y con muchos medios de transporte público disponibles para llegar fácilmente al centro de la ciudad.</p>" +
    "<img  class = 'center' src=\"../../Images/florence-hostel-common-area.jpg\" alt=\"La reception dell'ostello di Firenze\">"


export const ItalianTitle = "Ostello della gioventù di Firenze Villa Camerata | Camere Lowcost a Firenze "
export const EnglishTitle = "Florence Youth Hostel Villa Camerata | Lowcost rooms in Florence"
export const CroatianTitle = "Omladinski dom Firenca Villa Camerata | Sobe s niskim cijenama u Firenci"
export const TurkishTitle = "Florence Gençlik Merkezleri Villa Camerata | Floransa'da Lowcost odaları"
export const PortugueseTitle = "Albergue para jovens Florença Villa Camerata | Quartos lowcost em Florença"

export const ItalianDesc = "Il \"Villa Camerata\" è l'ostello ufficiale di Firenze (fa parte dell'associazione HI/AIG) ed è l'unico alloggio in città dove puoi trovare una bella atmosfera informale, buoni servizi e camere private o condivise a prezzi economici."
export const EnglishDesc = "The \"Villa Camerata\" is the official Florence hostel (it is part of the HI / AIG association) and it’s the only accommodation in town where you can find a nice informal atmosphere, good services and private or shared rooms at cheap prices."
export const CroatianDesc = "\"Villa Camerata\" je službeni hostel u Firenci (dio je udruge HI / AIG) i jedini je smještaj u gradu u kojem možete pronaći lijepu neformalnu atmosferu, dobre usluge i privatne ili zajedničke sobe po povoljnim cijenama."
export const TurkishDesc = "\"Villa Camerata\", Floransa'nın resmi hostelidir (HI / AIG birliğinin bir parçasıdır) ve şehirdeki tek güzel konaklama yeri, iyi hizmet ve ucuz ya da özel veya ortak odalar bulabileceğiniz konaklama birliğidir."
export const PortugueseDesc = "O \"Villa Camerata\" é o albergue oficial de Florença (faz parte da associação HI / AIG) e é o único alojamento na cidade onde você pode encontrar uma agradável atmosfera informal, bons serviços e quartos privativos ou compartilhados a preços baratos."

export const content = {"Portuguese" : Portuguese, "PortugueseTitle" : PortugueseTitle, "PortugueseDesc" : PortugueseDesc, "EnglishTitle" : EnglishTitle, "English" : English, "EnglishDesc" : EnglishDesc, "ItalianTitle" : ItalianTitle, "Italian" : Italian, "ItalianDesc" : ItalianDesc, "CroatianTitle" : CroatianTitle, "Croatian" : Croatian, "CroatianDesc" : CroatianDesc, "TurkishTitle" : TurkishTitle, "Turkish" : Turkish, "TurkishDesc" : TurkishDesc}