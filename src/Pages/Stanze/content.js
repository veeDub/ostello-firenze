export let Italian = '   <h3 class = \'mainTitle\'>Stanze e servizi</h3>\n' +
    '            <p class = "leftP">L\'ostello di Firenze dispone di varie possibilità di alloggio: si va dal classico poste letto in camerata da 4 (con dormitori separati per maschi e femmine) fino a stanze private singole, doppie (con letti singoli o matrimoniale), triple e quadruple con o senza bagno (ideali per famiglie o piccoli gruppi).</p>\n' +
    '            <p class = "leftP">La struttura dispone inoltre di camere singole e doppie con bagno situate nella zona camping.</p>\n' +
    '            <img class = \'center\' src = "../../Images/triple-room-florence-hostel.jpg" alt = \'Triple Room\' />' +
    '\n' +
    '            <p class = "leftP">Se soggiorni all\'ostello Villa Camerata potrai contare anche su ottimi servizi, come l\'<b>accesso a Internet</b> (wi-fi gratis su tutto l\'edificio) e la <b>colazione inclusa</b> nel prezzo.</p>\n' +
    '            <p class = "leftP">Tra gli spazi a disposizione degli ospiti ci sono un <b>bar</b>, un <b>ristorante</b>, una <b>lavanderia</b>, una <b>sala confereneze</b> e un parcheggio per biciclette, moto e auto.</p>\n' +
    '            <p class = "leftP">Nell\'ostello ci sono armadietti per i bagagli, ma non si possono chiudere a chiave: è comunque a disposizione degli ospiti una cassaforte per custodire gli <b>oggetti di valore</b>.</p>\n' +
    '            <img class = "center" src="../../Images/dorm-florence-hostel.jpg"  alt="A shared dorm at Florence Hostel">'

export let English = '   <h3 class = \'mainTitle\'>Rooms & Facilities</h3>\n' +
    '            <p class = "leftP">Florence hostel offers many accommodation solutions: from the classic bed in a 4 beds dorm in separate males and females sections to single, twin, double, triple and quad private rooms with or without bathroom.</p>' +
    '            <p class = "leftP">We also have double or single rooms with bathroom in our camping or much space outdoor if you are well equipped.</p>' +
    '            <img class = \'center\' src = "../../Images/triple-room-florence-hostel.jpg" alt = \'Triple Room\' />' +
    '\n' +
    '            <p class = "leftP">If you stay at the hostel Villa Camerata you can count on Internet access (free wi-fi available on all building) and breakfast included in the price.</p>' +
    '            <p class = "leftP">The spaces available to guests include a bar, a restaurant, a laundry, a conference room and parking for bikes and cars.</p>' +
    '            <p class = "leftP">In the hostel there are closets for baggages but it\'s not possible to lock them: there is anyway a safe for valuables.</p>' +
    '            <img class = "center" src="../../Images/dorm-florence-hostel.jpg"  alt="A shared dorm at Florence Hostel">'

export let Croatian = '   <h3 class = \'mainTitle\'>Sobe i Usluge</h3>' +
    '            <p class = "leftP">Hostel nudi mnoštvo smještajnih rješenja: od klasičnog kreveta u spavaonici s 4 kreveta u odvojenim odjelima za muškarce i žene do jednokrevetnih, dvokrevetnih, s bračnim krevetom, trokrevetnih i četverokrevetnih privatnih soba sa ili bez kupaonice.</p>' +
    '            <p class = "leftP">Također imamo dvokrevetne ili jednokrevetne sobe s kupaonicom u kampu ili mnogo prostora na otvorenom ako ste dobro opremljeni.\n' +
    '            <img class = \'center\' src = "../../Images/triple-room-florence-hostel.jpg" alt = \'Triple Room\' />' +
    '            <p class = "leftP">Ako odsjedate u hostelu Villa Camerata, možete računati na pristup internetu (besplatan wi-fi dostupan u svim zgradama) i doručak uključen u cijenu.</p>' +
    '            <p class = "leftP">Gostima na raspolaganju su bar, restoran, praonica rublja, sala za konferencije i parking za bicikle i automobile.</p>' +
    '            <p class = "leftP">U hostelu se nalaze ormari za prtljagu, ali ih nije moguće zaključati: postoji sef za dragocjenosti.</p>' +
    '            <img class = "center" src="../../Images/dorm-florence-hostel.jpg"  alt="A shared dorm at Florence Hostel">'

export const Turkish = '   <h3 class = \'mainTitle\'> Odalar & Tesis \n</h3>' +
    '            <p class = "leftP">Florence hostel, birçok konaklama çözümü sunmaktadır: 4 yataklı klasik erkek ve kadın ayrı yatakhane,  banyolu veya banyosuz tek, ikiz, çift, üç ve dört kişilik özel odalar.\n</p>' +
    '            <p class = "leftP">Ayrıca, eğer kampımızda banyolu çift kişilik veya tek kişilik odalarımız veya iyi donanımlıysanız, açık alanlarımız mevcuttur.\n' +
    '            <img class = \'center\' src = "../../Images/triple-room-florence-hostel.jpg" alt = \'Triple Room\' />' +
    '            <p class = "leftP">Hostel Villa Camerata\'da kalırsanız, İnternet erişimi (tüm binalarda ücretsiz wi-fi mevcuttur) ve fiyata dahil kahvaltıya güvenebilirsiniz.\n</p>' +
    '            <p class = "leftP">Konukların kullanabileceği alanlar arasında bir bar, restoran, çamaşırhane, konferans odası,  bisiklet ve araba park yeri vardır.\n</p>' +
    '            <p class = "leftP">Hostelde bagajlar için dolaplar var ancak bunları kilitlemek mümkün değil: yine de değerli eşyalar için bir kasa mevcuttur.\n</p>' +
    '            <img class = "center" src="../../Images/dorm-florence-hostel.jpg"  alt="A shared dorm at Florence Hostel">'

export const Portuguese = '   <h3 class = \'mainTitle\'> Habitaciones y servicios</h3>' +
    '            <p class = "leftP">El albergue de Florencia cuenta con varias opciones de alojamiento: desde la clásica habitación de 4 camas (con dormitorios separados para niños y niñas) hasta habitaciones individuales, dobles (con camas individuales o dobles), triples y cuádruples con o sin baño (ideal para familias o grupos pequeños).</p>' +
    '            <p class = "leftP">La propiedad también cuenta con habitaciones individuales y dobles con baño en la zona de acampada.' +
    '            <img class = \'center\' src = "../../Images/triple-room-florence-hostel.jpg" alt = \'Triple Room\' />' +
    '            <p class = "leftP">Si te alojas en el albergue Villa Camerata también puedes contar con excelentes servicios, como acceso a Internet (wi-fi gratuito en todo el edificio) y desayuno incluido en el precio.</p>' +
    '            <p class = "leftP">Dispone de bar, restaurante, lavandería, sala de conferencias y aparcamiento para bicicletas, motocicletas y coches.</p>' +
    '            <p class = "leftP">En el albergue hay taquillas para el equipaje, pero no se puede cerrar con llave: los huéspedes tienen a su disposición una caja fuerte para guardar objetos de valor.</p>' +
    '            <img class = "center" src="../../Images/dorm-florence-hostel.jpg"  alt="A shared dorm at Florence Hostel">'



export const ItalianTitle = "Camere private e dormitori economici a Firenze | Ostello Villa Camerata"
export const EnglishTitle = "Private rooms and cheap dorms in Florence | Hostel Villa Camerata"
export const CroatianTitle = "Privatne sobe i jeftine spavaonice u Firenci | Hostel Villa Camerata"
export const TurkishTitle = "Özel oda ve Floransa'da ucuz yatakhaneler | Hostel Villa Camerata"
export const PortugueseTitle = "Quartos particulares e dormitórios baratos em Florença | Hostel Villa Camerata"

export const ItalianDesc = "All’Ostello della Gioventù Villa Camerata di Firenze troverai sia camere private con o senza bagno, sia ampi dormitori condivisi, sempre con il miglior rapporto qualità/prezzo."
export const EnglishDesc = "At the Villa Camerata Youth Hostel in Florence you will find both private rooms with or without bathroom, and large shared dormitories, here you’ll find the best value for money."
export const CroatianDesc = "U Omladinskom domu Villa Camerata u Firenci naći ćete i privatne sobe sa ili bez kupaonice, te velike zajedničke spavaonice, uvijek s najboljim omjerom kvaliteta i cijena."
export const TurkishDesc = "Floransa'daki Villa Camerata Youth Hostel'de, her zaman en iyi kalite / fiyat oranına sahip, hem banyolu veya banyosuz özel odaları hem de geniş ortak yurtları bulacaksınız."
export const PortugueseDesc = "No Albergue da juventude Villa Camerata em Florença, você encontrará quartos privativos com ou sem banheiro e grandes dormitórios compartilhados, sempre com a melhor relação qualidade / preço."

export const content = {"Portuguese" : Portuguese, "PortugueseDesc" : PortugueseDesc, "PortugueseTitle" : PortugueseTitle, "EnglishTitle" : EnglishTitle, "English" : English, "EnglishDesc" : EnglishDesc, "ItalianTitle" : ItalianTitle, "Italian" : Italian, "ItalianDesc" : ItalianDesc, "CroatianTitle" : CroatianTitle, "Croatian" : Croatian, "CroatianDesc" : CroatianDesc, "TurkishTitle" : TurkishTitle, "Turkish" : Turkish, "TurkishDesc" : TurkishDesc}