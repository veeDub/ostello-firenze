import * as React from 'react'
import Page from "../Page/Page";
import {content} from './contents'
import Map from '../../Images/hostelMap.png'
import '../../index.css'

const Contatti = (props: any) =>{
    return (
        <div>
            <Page contentArray = {content} language = {props.language}/>
            <img className = 'center' src = {Map} alt = "Map" />
        </div>
    )
}

export default Contatti