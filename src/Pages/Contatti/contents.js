export let English = " <h3 class = 'mainTitle'>Location and Contact</h3>\n" +
    "                <p class='leftP'>Phone:\t0039 055 601451</p>\n" +
    "                <p class='leftP'>Fax:\t0039 055 610300</p>\n" +
    "                <p class='leftP'>Email:\tfirenze@aighostel.it</p>\n" +
    "                <p class='leftP'>Address:\tViale Augusto Righi n° 4, Florence, Tuscany, Italy</p>\n" +
    "                <h3 class='mainTitle'>HOW TO REACH FLORENCE HOSTEL VILLA CAMERATA</h3>\n" +
    "\n" +
    "                <p class='leftP'>Using the public transports:</p>\n" +
    "\n" +
    "                <p class='leftP'>From Monday to Saturday from 6 a.m. to 9.30 p.m. you can take the bus n° 11 from Florence main railway station (Santa Maria Novella) and from the city center. After 20 minutes get off at the stop \"Salviatino\". From the gate of the hostel walk up the hill for 600 metres. Every day, on Sundays and holidays from 6 a.m. to 00.30 a.m. there's the bus n° 17 to \"Santi Fiorentini\" stop (300 meters from \"Salviatino\" stop).\n" +
    "Take a look at <a href='www.ataf.net' >www.ataf.net</a> for more info and timetable.</p>\n" +
    "\n" +
    "                <p class='leftP'>If you arrive from international airport:</p>\n" +
    "\n" +
    "                <p class='leftP'>Amerigo Vespucci airport is 10 kms away and there's a shuttle bus to the Florence train staion.\n</p>\n" +
    "\n" +
    "                <p class='leftP'>If you arrive by car:\n</p>" +
    "\n" +
    "                <p class='leftP'>Amerigo Vespucci airport is 10 kms away and there's a shuttle bus to the Florence train staion./n Take the highway exut \"Firenze Sud\", 4 kms away from our hostel.\n In the Hostel you will find a variety of offers for guided tours of: Florence and its museums, palaces and gardens, the most important towns in Tuscany (Pisa, Siena, Lucca, Arezzo, San Gimignano, Volterra...), the most famous medieval villages (Monteriggioni, Certaldo...), the picturesque Tuscan countryside (Chianti, Casentino, Crete senesi...). \n And furthermore: samples and/or wine and food courses (wine tasting, cooking course..) or cultural ours (typical Florentine workshops) or shopping!</p>"

export let Italian = " <h3 class = 'mainTitle'>Contatti e posizione</h3>\n" +
    "                <p class='leftP'>Phone:\t0039 055 601451</p>\n" +
    "                <p class='leftP'>Fax:\t0039 055 610300</p>\n" +
    "                <p class='leftP'>Email:\tfirenze@aighostel.it</p>\n" +
    "                <p class='leftP'>Address:\tViale Augusto Righi n° 4, Florence, Tuscany, Italy</p>\n" +
    "                <p class='leftP'>COME RAGGIUNGERE L'OSTELLO DI FIRENZE CON I MEZZI PUBBLICI</p>\n" +
    "\n" +
    "                <p class='leftP'>Il bus 11 è quello che ferma più vicino all'ostello Villa Camerata: è operativo dal lunedì al sabato fino alle ore 21.30 e porta dalla Stazione Ferroviaria di Santa Maria Novella fino al capolinea Salviatino. Dal portone di entrata dell’ostello, percorri la salita che ti porterà al YHA Ostello di FIRENZE Villa Camerata (600 metri).</p>\n" +
    "\n" +
    "                <p class='leftP'>Altrimenti c'è il bus 17 disponibile tutti i giorni fino alle ore 00.30. Va dalla Stazione dei treni di Santa Maria Novella fino alla fermata di Santi Fiorentini.</p>\n" +
    "\n" +
    "                <p class='leftP'>Dall’ Aereoporto Internazionale Vespucci:</p>\n" +
    "\n" +
    "                <p class='leftP'>Utilizza lo shuttle che porta alla Stazione ferroviaria di Santa Maria Novella (10 km). Poi vedi le indicazioni riportate sopra.</p>\n" +
    "\n" +
    "                <p class='leftP'>In auto (autostrada)</p>\n" +
    "\n" +
    "                <p class='leftP'>L'uscita più vicina è Firenze Sud (a soli 4 chilometri dall'ostello).</p>\n"

export let Croatian = " <h3 class = 'mainTitle'>Lokacija i Kontakt</h3>\n" +
    "                <p class='leftP'>Telefon:    0039 055 601451</p>\n" +
    "                <p class='leftP'>Fax:    0039 055 610300</p>\n" +
    "                <p class='leftP'>Email:    firenze@aighostel.it</p>\n" +
    "                <p class='leftP'>Adresa:    Viale Augusto Righi n° 4, Firence, Toskana, Italija\n</p>\n" +
    "                <p class='leftP'>KAKO STIĆI DO FIRENTINSKOG HOSTEL VILLA CAMERATA</p>\n" +
    "\n" +
    "                <p class='leftP'>Ako stižete uz javni prijevoz:\n</p>\n" +
    "\n" +
    "                <p class='leftP'>Od ponedjeljka do subote od 18:00 do 21:30. autobusom broj 11 možete krenuti s glavnog željezničkog kolodvora u Firenci (Santa Maria Novella) i iz središta grada. Nakon 20 minuta siđite na stanici „Salviatino“. Od vrata hostela hodajte uzbrdo otprilike 600 metara. Svakog dana, nedjeljom i praznicima od 6 do 00, 30 sati, naći ćete autobus br. 17 do stanice \"Santi Fiorentini\" (300 metara od stajališta \"Salviatino\"). Pogledajte na www.ataf.net više informacija i voznog reda.\n</p>\n" +
    "\n" +
    "                <p class='leftP'>Ako stižete s međunarodne zračne luke:\n</p>\n" +
    "\n" +
    "                <p class='leftP'>Zračna luka Amerigo Vespucci udaljena je 10 km, a do željezničkog kolodvora u Firenci postoji shuttle autobus.\n</p>\n" +
    "\n" +
    "                <p class='leftP'>Ako dolazite autom:\n</p>\n" +
    "\n" +
    "                <p class='leftP'>Zračna luka Amerigo Vespucci udaljena je 10 km, a tamo postoji shuttle bus do željezničkog kolodvora u Firenci ./n Izađite na autoput \"Firenze Sud\", udaljen 4 km od našeg hostela. U hostelu ćete pronaći razne ponude za vodene obilaske: Firenca i njeni muzeji, palače i vrtovi, najvažniji gradovi Toskane (Pisa, Siena, Lucca, Arezzo, San Gimignano, Volterra ...), najpoznatija srednjovjekovna sela (Monteriggioni, Certaldo ...), slikoviti toskanski kraj (Chianti, Casentino, Crete senesi ...). I nadalje: uzorci i / ili tečajevi za vino i hranu (degustacija vina, tečaj kuhanja ..) ili kulturni tourovi (tipične firentinske radionice) ili kupovina!</p>\n"

export const Turkish = " <h3 class = 'mainTitle'>Konum & İletişim </h3>\n" +
    "                <p class='leftP'>Telefon:    0039 055 601451</p>\n" +
    "                <p class='leftP'>Faks:    0039 055 610300</p>\n" +
    "                <p class='leftP'>E-posta:    firenze@aighostel.it</p>\n" +
    "                <p class='leftP'>Adres:    Viale Augusto Righi n ° 4, Floransa, Toskana, İtalya\n</p>\n" +
    "                <p class='leftP'>FLORENCE HOSTEL VILLA CAMERATA'YA NASIL GİDİLİR\n</p>\n" +
    "\n" +
    "                <p class='leftP'>Toplu taşıma ile:\n</p>\n" +
    "\n" +
    "                <p class='leftP'>Pazartesiden cumartesiye sabah 6'dan akşam 9.30'a kadar 11 nolu otobüse Floransa ana tren istasyonundan (Santa Maria Novella) ve şehir merkezinden binebilirsiniz. 20 dakika sonra \"Salviatino\" durağında inin. Hostelin kapısından itibaren 600 metre tepeye yürüyün. Her gün, pazar ve tatil günlerinde 06:00 - 00:30 saatleri arasında, \"Santi Fiorentini\" durağında (\"Salviatino\" durağına 300 metre) duran 17 numaralı otobüs vardır. Daha fazla bilgi ve zaman çizelgesi için www.ataf.net sitesine bakınız.</p>\n" +
    "\n" +
    "                <p class='leftP'>Uluslararası havaalanından geliyorsanız:\n</p>\n" +
    "\n" +
    "                <p class='leftP'>Amerigo Vespucci havaalanı 10 km uzaklıktadır ve Floransa tren istasyonuna giden bir servis otobüsü vardır.\n</p>\n" +
    "\n" +
    "                <p class='leftP'>Araba ile geliyorsanız:\n</p>\n" +
    "\n" +
    "                <p class='leftP'>Amerigo Vespucci havaalanı 10 km uzaklıktadır ve Floransa tren istasyonuna giden bir servis otobüsü vardır. Otoyoldan \"Firenze Sud\" çıkışına girin, hostelimize 4 km uzaklıktadır. Hostel'de rehberli turlar için çeşitli teklifler bulacaksınız: Floransa ve müzeler, saraylar ve bahçeler, Toskana bölgesindeki en önemli şehirler (Pisa, Siena, Lucca, Arezzo, San Gimignano, Volterra ...), en ünlü ortaçağ köyleri (Monteriggioni, Certaldo ...), pitoresk Toskana kırsalları (Chianti, Casentino, Girit senesi ...). Ve ayrıca: şarap ve yemek kursları (şarap tadımı, yemek pişirme kursu ..) ya da kültürel atölyeler (tipik Floransa atölyeleri) ya da alışveriş!</p>\n"

export const Portuguese = " <h3 class = 'mainTitle'>Contacto y Ubicación</h3>\n" +
    "                <p class='leftP'>Teléfono: 0039 055 601451</p>\n" +
    "                <p class='leftP'>Fax: 0039 055 610300</p>\n" +
    "                <p class='leftP'>Correo electrónico: firenze@aighostel.it</p>\n" +
    "                <p class='leftP'>Dirección: Viale Augusto Righi n° 4, Florencia, Toscana, Italia</p>\n" +
    "                <p class='leftP'>CÓMO LLEGAR AL ALBERGUE DE FLORENCIA EN TRANSPORTE PÚBLICO</p>\n" +
    "\n" +
    "                <p class='leftP'>El autobús 11 es la parada de autobús más cercana al albergue Villa Camerata: funciona de lunes a sábado hasta las 21.30 horas y te lleva desde la estación de ferrocarril de Santa Maria Novella hasta la terminal de Salviatino. Desde la puerta de entrada del albergue, sube la colina que te llevará al albergue Ostello di FIRENZE Villa Camerata (600 metros).</p>\n" +
    "\n" +
    "                <p class='leftP'>De lo contrario, hay un autobús 17 disponible todos los días hasta las 00.30 horas. Va desde la estación de tren de Santa Maria Novella hasta la parada de Santi Fiorentini.\n</p>\n" +
    "\n" +
    "                <p class='leftP'>Desde el Aeropuerto Internacional de Vespucci:</p>\n" +
    "\n" +
    "                <p class='leftP'>Coger el autobús de enlace a la estación de ferrocarril de Santa Maria Novella (10 km). Luego vea las instrucciones de arriba.</p>\n" +
    "\n" +
    "                <p class='leftP'>En coche (autopista)</p>\n" +
    "\n" +
    "                <p class='leftP'>La salida más cercana es Firenze Sud (a sólo 4 km del albergue).</p>\n"


export const ItalianTitle = "Contatta lo staff dell’Ostello Villa Camerata a Firenze"
export const EnglishTitle = "Contact the Villa Camerata Hostel staff in Florence "
export const CroatianTitle = "Obratite se osoblju Villa Camerata Hostel u Firenci"
export const TurkishTitle = "Floransa'daki Villa Camerata Hostel personeli ile iletişime geçin"
export const PortugueseTitle = "Entre em contato com a equipe do Villa Camerata Hostel em Florença"

export const ItalianDesc = "L’ostello di Firenze si trova ai piedi delle colline di Fiesole, in una villa del 15° secolo decorata con stucchi e affreschi e circondata da un parco. E’ vicino a una piscina pubblica, allo stadio di calcio Artemio Franchi e al Nelson Mandela Forum."
export const EnglishDesc = "The Florence hostel is located at the foot of the Fiesole hills, in a 15th century villa decorated with stucco and frescoes and surrounded by a park. It is close to a public swimming pool, the Artemio Franchi football stadium and the Nelson Mandela Forum."
export const CroatianDesc = "Hostel Firenca smješten je u podnožju brda Fiesole, u vili iz 15. stoljeća ukrašenoj štukaturama i freskama i okružen parkom. U blizini je javni bazen, nogometni stadion Artemio Franchi i Forum Nelson Mandela."
export const TurkishDesc = "Florence hosteli, Fiesole tepelerinin eteğinde, sıva ve fresklerle dekore edilmiş ve bir parkla çevrili 15. yüzyıldan kalma bir villada yer almaktadır. Halka açık yüzme havuzuna, Artemio Franchi futbol stadyumuna ve Nelson Mandela Forumuna yakındır."
export const PortugueseDesc = "O albergue de Florença está localizado no sopé das colinas de Fiesole, em uma vila do século XV decorada com estuque e afrescos e cercada por um parque. Fica perto de uma piscina pública, do estádio de futebol Artemio Franchi e do Fórum Nelson Mandela."

export const content = {"PortugueseDesc" : PortugueseDesc, "PortugueseTitle" : PortugueseTitle, "Portuguese": Portuguese, "EnglishTitle" : EnglishTitle, "English" : English, "EnglishDesc" : EnglishDesc, "ItalianTitle" : ItalianTitle, "Italian" : Italian, "ItalianDesc" : ItalianDesc, "CroatianTitle" : CroatianTitle, "Croatian" : Croatian, "CroatianDesc" : CroatianDesc, "TurkishTitle" : TurkishTitle, "Turkish" : Turkish, "TurkishDesc" : TurkishDesc}
