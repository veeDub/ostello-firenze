import * as React from 'react'
import Helmet from "react-helmet";

const Page = (props) => {
    const [text, setText] = React.useState("Italian");
    const [titleMeta, setTitleMeta] = React.useState("Italian")
    const [descMeta, setDescMeta] = React.useState("Italian")


    React.useEffect(() => {
        let content = props.contentArray
        if(props.language === "English") {
            setText(content.English)
            setTitleMeta(content.EnglishTitle)
            setDescMeta(content.EnglishDesc)
        }else if(props.language === "Italian"){
            setText(content.Italian)
            setTitleMeta(content.ItalianTitle)
            setDescMeta(content.ItalianDesc)
        }else if(props.language === "Croatian"){
            setText(content.Croatian)
            setTitleMeta(content.CroatianTitle)
            setDescMeta(content.CroatianDesc)
        }else if(props.language === "Turkish"){
            setText(content.Turkish)
            setTitleMeta(content.TurkishTitle)
            setDescMeta(content.TurkishDesc)
        }else if(props.language === "Portuguese"){
            setText(content.Portuguese)
            setTitleMeta(content.PortugueseTitle)
            setDescMeta(content.PortugueseDesc)
        }
    }, [props.language, props.contentArray])

    return (
        <div>
            <Helmet>
                <title>{titleMeta}</title>
                <meta name = "description" content = {descMeta} />
            </Helmet>
            <div dangerouslySetInnerHTML={{ __html: text }} />
        </div>
    )
}

export default Page