import * as React from 'react'

const LanguageSelect = (props) =>{
    // send language var up to parent
    const sendLang = (e) => {
        props.onChangeFunc(e.target.value)
    }
    return (
        <select onChange={sendLang}>
            <option value = "Italian">Italian</option>
            <option value = "English">English</option>
            <option value = "Croatian">Croatian</option>
            <option value = "Turkish">Turkish</option>
            <option value = "Portuguese">Portuguese</option>
        </select>
    )
}

export default LanguageSelect