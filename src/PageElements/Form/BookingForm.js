import React from "react";
import DatePicker, { registerLocale } from "react-datepicker"
// CSS Modules, react-datepicker-cssmodules.css
import 'react-datepicker/dist/react-datepicker-cssmodules.css'
import "react-datepicker/dist/react-datepicker.css"
import it from "date-fns/locale/it"

registerLocale("it", it)


class BookingForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 3,
            startDate: new Date(),
            day: 0,
            month: 0,
            year: 0,
            nights: 3
        };
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleNightChange = this.handleNightChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    createOptionArray(num){
        let returnArray = []
        for(let i = 1; i < num + 1; i++){
            returnArray.push(i)
        }
        return returnArray
    }

    createOptionItem(num){
        return  <option value = {num} key = {num}> {num} </option>
    }

    handleDateChange(date) {
        this.setState({
            startDate: date,
            day: date.getDate(),
            month: date.getMonth() + 1,
            year: date.getFullYear()
        });
    }
    handleNightChange(event){
        this.setState({value: event.target.value, nights : parseInt(event.target.value)})
    }

    handleSubmit(){
        // https://www.reservationarea.com/str/6094/it/step02_str.php?currency=1&giorno=23&mese=8&anno=2019&notti=3&ref=http%3A%2F%2Fostellofirenze.it%2Findex.html
        window.open(
            "https://www.reservationarea.com/str/6094/it/step02_str.php?currency=1&giorno=" + this.state.day + "&mese=" + this.state.month + "&anno=" + this.state.year + "&notti=" + this.state.nights + "&ref=http%3A%2F%2Fostellofirenze.it%2Findex.html" ,
            '_self'
        );
    }

    returnNightByLanguage(language){
        if(language === "English") {
            return "Nights: "
        }else if(language === "Italian"){
            return "Notti: "
        }else if(language === "Croatian"){
            return "Noći: "
        }else if(language === "Turkish"){
            return "Gece: ";
        }else if(language === "Portuguese"){
            return "Noches: "
        }
    }

    returnSubmitByLanguage(language){
        if(language === "English") {
            return "Submit "
        }else if(language === "Italian"){
            return "Sottoscrivi"
        }else if(language === "Croatian"){
            return "Podnijeti"
        }else if(language === "Turkish"){
            return "Gönder"
        }else if(language === "Portuguese"){
            return "Enviar"
        }
    }

    returnMonthsByLanguage(language){
        return (language === "Italian" ?  "it" :  "")
    }

    componentDidMount() {
        // we need to set date right away just in case user does not choose a different day
        // otherwise it errors at reservationarea.com
        this.handleDateChange(new Date())
    }
    //action= {"https://www.reservationarea.com/str/6094/it/step02_str.php?currency=1&giorno=" + this.state.day + "&mese=" + this.state.month + "&anno=" + this.state.year + "&notti=" + this.state.nights + "&ref=http%3A%2F%2Fostellofirenze.it%2Findex.html"}
    render() {
        return (
            <div>
                <label><b>Check in :</b></label>
                <br />
                <br />
                <br />
                <DatePicker
                    selected={this.state.startDate}
                    onChange={this.handleDateChange}
                    locale = {this.returnMonthsByLanguage(this.props.language)}
                />
                <br />
                <br />
                <label><b>{this.returnNightByLanguage(this.props.language)} </b></label>
                <br />
                <br />
                <select value={this.state.value} onChange= {this.handleNightChange}>
                    {this.createOptionArray(30).map( num => this.createOptionItem(num) )}
                </select>
                <br />
                <br />
                <br />
                <input type="submit" value={this.returnSubmitByLanguage(this.props.language)} onClick = {this.handleSubmit}  />
            </div>
        );
    }
}

export default BookingForm