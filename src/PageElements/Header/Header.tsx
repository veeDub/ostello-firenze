import * as React from 'react'
import {Link} from 'react-router-dom'
import './Header.css';


const Header = (props : any) => {
    return(
        <header>
            <br />
            <h1 > {props.title} </h1>
            <nav className = 'center'>
                <ul className = {props.ulCSS}>
                    <li className = {props.liCSS}>
                        <Link to= '/ostello'  className = 'menu-link'> {props.buttons[0]} </Link>
                    </li>
                    <li className = {props.liCSS}>
                        <Link to = '/stanze' className = 'menu-link'> {props.buttons[1]} </Link>
                    </li>
                    <li className = {props.liCSS}>
                        <Link to = '/prenota' className = 'menu-link'> {props.buttons[2]} </Link>
                    </li>
                    <li className = {props.liCSS}>
                        <Link to = '/galleria' className = 'menu-link'> {props.buttons[3]} </Link>
                    </li>
                    <li className = {props.liCSS}>
                        <Link to = '/contatti' className = 'menu-link'> {props.buttons[4]} </Link>
                    </li>
                </ul>
            </nav>
        </header>
    )

}
export default Header