import ImageGallery from 'react-image-gallery';
import * as React from 'react'
import "react-image-gallery/styles/css/image-gallery.css";


let oneOriginal = require('../../Images/Original/one.png')
let oneThumb = require('../../Images/Thumbnails/one.png')

let twoOriginal = require('../../Images/Original/two.png')
let twoThumb = require('../../Images/Thumbnails/two.png')

let threeOriginal = require('../../Images/Original/three.png')
let threeThumb = require('../../Images/Thumbnails/three.png')

let fourOriginal = require('../../Images/Original/four.png')
let fourThumb = require('../../Images/Thumbnails/four.png')

let fiveOriginal = require('../../Images/Original/five.png')
let fiveThumb = require('../../Images/Thumbnails/five.png')

let sixOriginal = require('../../Images/Original/six.png')
let sixThumb = require('../../Images/Thumbnails/six.png')

let sevenOriginal = require('../../Images/Original/seven.png')
let sevenThumb = require('../../Images/Thumbnails/seven.png')

let eightOriginal = require('../../Images/Original/eight.png')
let eightThumb = require('../../Images/Thumbnails/eight.png')

class Gallery extends React.Component {

    render() {

        const images = [
            {
                original: oneOriginal,
                thumbnail: oneThumb,
            },
            {
                original: twoOriginal,
                thumbnail: twoThumb
            },
            {
                original: threeOriginal,
                thumbnail: threeThumb
            },
            {
                original: fourOriginal,
                thumbnail: fourThumb
            },
            {
                original: fiveOriginal,
                thumbnail: fiveThumb
            },
            {
                original: sixOriginal,
                thumbnail: sixThumb
            },
            {
                original: sevenOriginal,
                thumbnail: sevenThumb
            },
            {
                original: eightOriginal,
                thumbnail: eightThumb
            }
        ]

        return (
            <ImageGallery items={images} />
        );
    }

}


export default Gallery