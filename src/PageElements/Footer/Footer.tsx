import * as React from 'react'
import '../../index.css'

const Footer = ()=>{
    return (
        <p className = 'footer'>Site created by
            <a href="http://www.hostelsclub.com/">
                <img className = 'footer_image' src="https://www.hostelsclub.com/assets/img/logo.png" alt="HostelsClub"/>
            </a>
        </p>
    )
}

export default Footer