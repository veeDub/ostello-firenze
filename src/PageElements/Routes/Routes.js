import * as React from 'react'
import {CSSTransition, TransitionGroup} from "react-transition-group";
import {Redirect, Route, Switch} from "react-router";

import Contatti from "../../Pages/Contatti/Contatti";
import Galleria from "../../Pages/Galleria/Galleria";
import Ostello from "../../Pages/Ostello/Ostello";
import NotFoundPage from "../../Pages/NotFound/NotFound";
import Prenota from "../../Pages/Prenota/Prenota";
import Stanze from "../../Pages/Stanze/Stanze";


const Routes = (props) => {
    return (
        <div>
            <br />
            <br />
            <div className = {props.classCSS}>
                <TransitionGroup>
                    <CSSTransition
                        key={props.locationKey}
                        timeout={500}
                        classNames="animate"
                    >
                        <Switch>
                            <Redirect exact={true} from="/" to="/ostello/" />
                            <Route exact={true} path="/contatti" component={ ()=> <Contatti language = {props.language} />} />
                            <Route exact = {true} path= "/galleria" component ={ ()=> <Galleria language = {props.language} />} />
                            <Route exact = {true} path="/ostello/" component={ ()=> <Ostello language = {props.language} />} />
                            <Route exact = {true} path="/prenota" component={ ()=> <Prenota language = {props.language} />} />
                            <Route exact = {true} path="/stanze" component={ ()=> <Stanze language = {props.language} />} />

                            <Route component={NotFoundPage} />
                        </Switch>
                    </CSSTransition>
                </TransitionGroup>
            </div>
        </div>
    )
}

export default Routes