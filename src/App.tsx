import * as React from "react";
import './index.css';
import {Helmet} from 'react-helmet'
import { BrowserRouter as Router, Route } from "react-router-dom";

import Header from "./PageElements/Header/Header"
import Footer from "./PageElements/Footer/Footer";
import LanguageSelect from "./PageElements/LanguageSelect/LanguageSelect";
import Routes from "./PageElements/Routes/Routes";

const RoutesWrap: React.FunctionComponent = () => {
    return (
        <Router>
            <Route component={App} />
        </Router>
    );
};

const App = (props : any) => {
    const buttonsEnglish = ["About Florence Hostel", "Rooms & Facilities", "Online Booking", "Photogallery", "Location & Contact"]
    const buttonsItalian = ["Ostello", "Stanze e Servizi", "Prenota Online", "Galleria", "Contatti e Posizione"]
    const buttonsCroatian = ["O Hostelu", "Sobe i Usluge", "Online Rezervacije", "Fotogalerija", "Lokacija i Kontakt"]
    const buttonsTurkish = ["Florance Hostel Hakkında", "Odalar & Tesis", "Online Rezervasyon", "Galeri", "Konum & İletişim"]

    const [language, setLanguage] = React.useState("Italian")
    const [ulCssName, setUlCss] = React.useState("")
    const [liCssName, setLiCss] = React.useState("")
    const [title, setTitle] = React.useState("Ostello di Firenze Villa Camerata")
    const [buttons, setButtons] = React.useState(buttonsItalian)

    const setAllLangState = ( language : string, title : string, buttons : string[])=>{
        setLanguage(language)
        setTitle(title)
        setButtons(buttons)
    }
    const setAllCssState = ( ul : string, li : string) => {
        setUlCss(ul)
        setLiCss(li)
    }

    const setLang = (language : string)=>{
        // get language from child language select
        // then set all components to appropriate language
        setLanguage(language)
        if(language === "English"){
            setAllLangState("English", "Villa Camerata Hostel", buttonsEnglish)
        }else if(language === "Italian"){
            setAllLangState("Italian", "Ostello di Firenze Villa Camerata", buttonsItalian)
        }else if(language === "Croatian"){
            setAllLangState("Croatian", "Hostel Firenze Villa Camerata", buttonsCroatian)
        }else if(language === "Turkish"){
            setAllLangState("Turkish", "Villa Camerata Hostel", buttonsTurkish)
        }

    }

    const updateDimensions = () => {
        ( window.innerWidth < 410 )
            ? setAllCssState('ulVertical', "liVertical")
            : setAllCssState('ulHorizontal', "liHorizontal")
    }

    React.useEffect(() => {
        updateDimensions()
        window.addEventListener("resize", updateDimensions);
    })


    return (
            <div>
                <br />
                <br />
                    <div className = 'center'>
                        <Helmet>
                            <meta charSet= "utf-8" />
                            <title>Ostello Firenze</title>
                            <link rel="canonical" href="https://ostellofirenze.it" />
                        </Helmet>
                        <div className = "outerDiv">
                            <Header title = {title} ulCSS={ ulCssName } liCSS = { liCssName } buttons = { buttons }/>
                            <div className = "innerDiv">
                                <LanguageSelect onChangeFunc = {setLang}/>
                                <Routes language = { language }  locationKey = {props.location.key} classCSS  = "innerTextDiv"/>
                                <Footer/>
                            </div>
                        </div>
                    </div>
                 </div>
    );
};

export default RoutesWrap;
